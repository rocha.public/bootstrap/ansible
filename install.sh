#!/bin/bash
set -e
bootstrap_path=$1

echo -e ================== '\033[0;32m'Installing Ansible'\033[0m' ==================
apt-get install -y software-properties-common
# echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu bionic main" >> /etc/apt/sources.list
# echo "deb-src http://ppa.launchpad.net/ansible/ansible/ubuntu bionic main" >> /etc/apt/sources.list
apt-get update
apt-get install -y --force-yes ansible
cp ${bootstrap_path}ansible.cfg /etc/ansible/ansible.cfg
