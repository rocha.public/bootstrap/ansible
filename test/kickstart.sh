#!/bin/bash
set -e
SNIPPET_ID=1690593

echo -e ================== '\033[0;32m'Set Environemt Variables'\033[0m' ==================
export VM_PROXY='http://_ldapuser:C!elo_2016@proxycielo.visanet.corp:8080'
export VM_NO_PROXY_HOSTS='localhost,127.0.0.1,sigo-git.ccorp.local,sigo-jenkins.ccorp.local,vvcelprd0170,VVCELPRD0170.ccorp.local,192.168.56.*,*.visanet.corp,*.ccorp.local'
export VM_INTERNAL_GIT=http://sigo-git.ccorp.local/
export VM_INTERNAL_JENKINS=http://sigo-jenkins.ccorp.local:8080/

echo -e ================== '\033[0;32m'Setup Start'\033[0m' ==================
curl https://gitlab.com/snippets/${SNIPPET_ID}/raw -o setup.temp.sh
./setup.temp.sh --has_proxy true
